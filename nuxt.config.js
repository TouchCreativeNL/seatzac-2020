/* eslint-disable @typescript-eslint/no-unused-vars */
export default {
  target: 'server',
  server: {
    port: 3001,
  },
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  router: {
    linkActiveClass: 'active-menu-item',
    prefetchLinks: false,
    base: '/',
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition
      }

      return { x: 0, y: 0 }
    },
  },
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    htmlAttrs: {
      lang: 'nl',
    },
    title: 'SeatZac - Chill wherever',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0, viewport-fit=cover',
      },
      {
        hid: 'description',
        name: 'description',
        content:
          'De compacte en super comfortabele lucht gevulde chillbag die je overal mee naartoe kunt nemen. Chill als een baas.',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Global CSS
   */
  css: ['@/assets/styles/main.scss', '@/assets/fonts/bebas/bebas.scss'],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    { src: '~/plugins/vue-awesome-swiper', ssr: false },
    { src: '~/plugins/in-viewport', mode: 'client' },
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxt/typescript-build',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-80535044-1',
      },
    ],
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxt/content',
    '@nuxtjs/apollo',
    '@nuxtjs/axios',
    '@nuxtjs/toast',
  ],
  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint:
          'https://seatzactm.myshopify.com/api/2021-01/graphql.json',
        httpLinkOptions: {
          headers: {
            'Content-Type': 'application/json',
            'X-Shopify-Storefront-Access-Token':
              'c7c716b9d14c9cfae9c332245eb4cc9f',
          },
        },
      },
    },
  },
  toast: {
    position: 'top-center',
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    transpile: ['gsap'],
    analyze: false,
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
}
