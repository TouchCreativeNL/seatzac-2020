import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _51c6e8e2 = () => interopDefault(import('../pages/winkelwagen/index.vue' /* webpackChunkName: "pages/winkelwagen/index" */))
const _0769c7f2 = () => interopDefault(import('../pages/business/_slug/index.vue' /* webpackChunkName: "pages/business/_slug/index" */))
const _1a3ac871 = () => interopDefault(import('../pages/seatzac/_slug/index.vue' /* webpackChunkName: "pages/seatzac/_slug/index" */))
const _41cdc52f = () => interopDefault(import('../pages/service/_slug/index.vue' /* webpackChunkName: "pages/service/_slug/index" */))
const _028ead01 = () => interopDefault(import('../pages/terms/_slug/index.vue' /* webpackChunkName: "pages/terms/_slug/index" */))
const _26af7a48 = () => interopDefault(import('../pages/seatzac/_slug/_id/index.vue' /* webpackChunkName: "pages/seatzac/_slug/_id/index" */))
const _450e87ce = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'active-menu-item',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/winkelwagen",
    component: _51c6e8e2,
    name: "winkelwagen"
  }, {
    path: "/business/:slug",
    component: _0769c7f2,
    name: "business-slug"
  }, {
    path: "/seatzac/:slug",
    component: _1a3ac871,
    name: "seatzac-slug"
  }, {
    path: "/service/:slug",
    component: _41cdc52f,
    name: "service-slug"
  }, {
    path: "/terms/:slug",
    component: _028ead01,
    name: "terms-slug"
  }, {
    path: "/seatzac/:slug?/:id",
    component: _26af7a48,
    name: "seatzac-slug-id"
  }, {
    path: "/",
    component: _450e87ce,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
