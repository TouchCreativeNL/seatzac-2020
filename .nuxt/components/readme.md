# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Categorychairslide>` | `<categorychairslide>` (components/Categorychairslide.vue)
- `<Chairslide>` | `<chairslide>` (components/Chairslide.vue)
- `<Checkoutbutton>` | `<checkoutbutton>` (components/Checkoutbutton.vue)
- `<Cookiebar>` | `<cookiebar>` (components/Cookiebar.vue)
- `<Productbox>` | `<productbox>` (components/Productbox.vue)
- `<Footer>` | `<footer>` (components/footer.vue)
- `<Header>` | `<header>` (components/header.vue)
- `<Heading>` | `<heading>` (components/heading.vue)
