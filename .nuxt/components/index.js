export { default as Categorychairslide } from '../../components/Categorychairslide.vue'
export { default as Chairslide } from '../../components/Chairslide.vue'
export { default as Checkoutbutton } from '../../components/Checkoutbutton.vue'
export { default as Cookiebar } from '../../components/Cookiebar.vue'
export { default as Productbox } from '../../components/Productbox.vue'
export { default as Footer } from '../../components/footer.vue'
export { default as Header } from '../../components/header.vue'
export { default as Heading } from '../../components/heading.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
