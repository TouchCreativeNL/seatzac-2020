/* eslint-disable import/no-mutable-exports */
import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import Shop from '~/store/shop'

let shopStore: Shop

function initialiseStores(store: Store<any>): void {
  shopStore = getModule(Shop, store)
}

export { initialiseStores, shopStore }
