/* eslint-disable object-shorthand */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-undef */
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import gqlGetCheckout from '~/apollo/queries/gqlGetCheckout.gql'
import gqlAddLineItem from '~/apollo/queries/gqlAddLineItem.gql'
import gqlGetCart from '~/apollo/queries/gqlGetCart.gql'
import gqlDeleteLineItem from '~/apollo/queries/gqlDeleteLineItem.gql'
import { shopStore } from '~/utils/store-accessor'

@Module({
  stateFactory: true,
  namespaced: true,
  name: 'shop',
})
export default class Shop extends VuexModule {
  count = 1

  @Mutation
  increment(delta: number) {
    this.count = delta
  }

  @Action({ commit: 'increment' })
  incr() {}

  @Mutation
  async generateCheckoutIDAndAddLineItem(variantId) {
    try {
      // @ts-ignore
      await $nuxt.$apollo.mutate({
        mutation: gqlGetCheckout,

        async update(store, { data: { checkoutCreate } }) {
          await shopStore.addLineItem({
            variantId: variantId,
            checkoutId: checkoutCreate.checkout.id,
          })
          // @ts-ignore
          $nuxt.$router.push({
            // @ts-ignore
            path: $nuxt.$route.path,
            query: {
              checkoutId: checkoutCreate.checkout.id,
            },
          })

          // @ts-ignore
          $nuxt.$toasted.show('Generating CheckoutId').goAway(800)
        },
      })
    } catch (error) {
      // @ts-ignore
      this.$toasted.show(error).goAway(800)
    }
  }

  @Mutation
  async generateCheckoutIDAndAddLineItemHome(variantId) {
    try {
      // @ts-ignore
      await $nuxt.$apollo.mutate({
        mutation: gqlGetCheckout,

        async update(store, { data: { checkoutCreate } }) {
          await shopStore.addLineItemHome({
            variantId: variantId,
            checkoutId: checkoutCreate.checkout.id,
          })

          // @ts-ignore
          $nuxt.$router.push({
            // @ts-ignore
            path: $nuxt.$route.path,
            query: {
              checkoutId: checkoutCreate.checkout.id,
            },
          })

          setTimeout(() => {
            // @ts-ignore
            $nuxt.$router.push({
              // @ts-ignore
              path: '/winkelwagen',
              query: {
                // @ts-ignore
                checkoutId: $nuxt.$route.query.checkoutId,
              },
            })
          }, 500)

          // @ts-ignore
          $nuxt.$toasted.show('Generating CheckoutId').goAway(800)
        },
      })
    } catch (error) {
      // @ts-ignore
      this.$toasted.show(error).goAway(800)
    }
  }

  @Mutation
  async addLineItem(addItem: any) {
    const checkoutId = addItem.checkoutId
    const variantID = addItem.variantId

    try {
      // @ts-ignore
      await $nuxt.$apollo.mutate({
        mutation: gqlAddLineItem,
        variables: {
          // @ts-ignore
          checkoutId: checkoutId,
          lineItems: [
            {
              quantity: this.count,
              variantId: `${variantID}`,
            },
          ],
        },
        update(store, { data: { checkoutLineItemsAdd } }) {
          try {
            const data = store.readQuery({
              query: gqlGetCart,
              // @ts-ignore
              variables: { id: checkoutId },
            })

            data.node.lineItems = checkoutLineItemsAdd.checkout.lineItems
            data.node.totalPrice = parseInt(
              checkoutLineItemsAdd.checkout.lineItemsSubtotalPrice.amount
            )

            store.writeQuery({
              query: gqlGetCart,
              // @ts-ignore
              variables: { id: checkoutId },
              data,
            })

            // @ts-ignore
            $nuxt.$toasted.success('Adding to cart!').goAway(800)
            return
          } catch (error) {
            // @ts-ignore
            console.log(error)
            // $nuxt.$toasted.error(error).goAway(800)
          }
        },
      })
    } catch (error) {
      // @ts-ignore
      console.log(error)
      // $nuxt.$toasted.error(error).goAway(800)
    }
  }

  @Mutation
  async deleteLineItem(variantID: any) {
    try {
      // @ts-ignore
      await $nuxt.$apollo.mutate({
        mutation: gqlDeleteLineItem,
        variables: {
          // @ts-ignore
          checkoutId: $nuxt.$route.query.checkoutId,
          lineItemIds: `${variantID}`,
        },
        update: (store, { data: { checkoutLineItemsRemove } }) => {
          try {
            const data = store.readQuery({
              query: gqlGetCart,
              // @ts-ignore
              variables: { id: $nuxt.$route.query.checkoutId },
            })

            const index = data.node.lineItems.edges.findIndex(
              (edge) => edge.node.id === variantID
            )
            if (index > -1) {
              data.node.lineItems.edges.splice(index, 1)
            }
            data.node.totalPrice =
              checkoutLineItemsRemove.checkout.lineItemsSubtotalPrice.amount

            store.writeQuery({
              query: gqlGetCart,
              // @ts-ignore
              variables: { id: $nuxt.$route.query.checkoutId },
              data,
            })

            // @ts-ignore
            $nuxt.$toasted.error('Removed from cart!').goAway(800)
          } catch (error) {
            // @ts-ignore
            $nuxt.$toasted.error(error).goAway(800)
          }
        },
      })
    } catch (error) {
      // @ts-ignore
      $nuxt.$toasted.error(error).goAway(800)
    }
  }

  @Mutation
  async addLineItemHome(addItem: any) {
    const checkoutId = addItem.checkoutId
    const variantId = addItem.variantId

    try {
      // @ts-ignore
      await $nuxt.$apollo.mutate({
        mutation: gqlAddLineItem,
        variables: {
          // @ts-ignore
          checkoutId: checkoutId,
          lineItems: [
            {
              quantity: 1,
              variantId: `${variantId}`,
            },
          ],
        },
        update(store, { data: { checkoutLineItemsAdd } }) {
          try {
            const data = store.readQuery({
              query: gqlGetCart,
              // @ts-ignore
              variables: { id: checkoutId },
            })

            data.node.lineItems = checkoutLineItemsAdd.checkout.lineItems
            data.node.totalPrice = parseInt(
              checkoutLineItemsAdd.checkout.lineItemsSubtotalPrice.amount
            )

            store.writeQuery({
              query: gqlGetCart,
              // @ts-ignore
              variables: { id: checkoutId },
              data,
            })

            // @ts-ignore
            $nuxt.$toasted.success('Adding to cart!').goAway(800)

            // @ts-ignore
            $nuxt.$router.push({
              // @ts-ignore
              path: '/winkelwagen',
              query: {
                checkoutId: checkoutId,
              },
            })
            return
          } catch (error) {
            // @ts-ignore
            console.log(error)
            // $nuxt.$toasted.error(error).goAway(800)
          }
        },
      })
    } catch (error) {
      // @ts-ignore
      $nuxt.$toasted.error(error).goAway(800)
    }
  }
}
