---
title: Return & Exchange
---

<b>Reflection period</b><br />
For your SeatZac orders, a trial period of 14 days applies from the moment the order is in your possession. Within this period you can view, assess and return the item undamaged.

If you are not satisfied with the item or the product is damaged on delivery, you can return it. The item must be in its original condition in its factory packaging.<br />

<b>You can sent it to:</b><br />

ZigZac BV<br />
Brantjesstraat 72<br />
1447 PE Purmerend<br />

SeatZac will pay the shipping costs to the customers address. The shipping cost for the returning orders are paid by the customer.
