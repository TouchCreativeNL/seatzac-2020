---
title: FAQ
questions:
  [
    {
      _id: 0,
      questions: Is there a warranty on this item?,
      answer: Yes&comma; all according EU regulations.,
    },
    {
      _id: 1,
      questions: How long does it take for the air to inflate?,
      answer: On average < 60 seconds.,
    },
    {
      _id: 2,
      questions: Can I use it on the water?,
      answer: No&comma; it’s only water resistant&comma; not waterproof.,
    },
    { _id: 3, questions: Can SEATZAC charge my phone?, answer: Yes. },
    {
      _id: 4,
      questions: When to start shipping?,
      answer: You will receive a tracking code 1-2 days after your order. Did not receive it? Contact our customer service,
    },
    {
      _id: 5,
      questions: How do I fill my SeatZac E-ZY?,
      answer: If you will follow the above procedure your SeatZac will keep the air longer inside. The air from the SeatZac always decreases a little after standing and sitting several times. Simply press the orange fresh air button to replenish your SeatZac and chill on!,
      answerList:
        [
          '1. Roll up the opening 5 times very tight',
          '2. Click/close the buckle',
          '3. Screw of the left cap of the MR E-ZY',
          '4. Press the orange button',
          '5. When your SeatZac is filled up with air screw the cap back on.',
        ],
    },
    {
      _id: 6,
      questions: How do I fill my SeatZac E-ZY Lounger?,
      answer: If you will follow the above procedure your SeatZac will keep the air longer inside. The air from the SeatZac always decreases a little after standing and sitting several times. Simply press the orange fresh air button to replenish your SeatZac and chill on!,
      answerList:
        [
          '1. Roll up the opening 7/8 times very tight',
          '2. Click/close the buckle',
          '3. Screw of the left cap of the MR E-ZY',
          '4. Press the orange button',
          '5. When your SeatZac is filled up with air screw the cap back on.',
        ],
    },
  ]
---
