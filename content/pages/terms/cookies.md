---
title: Cookies
---

<b>Cookies</b> At SeatZac, we respect your concerns about privacy and value the relationship that we have with you. Like many companies, we use technology on our website to collect information that helps us enhance your experience and our products and services. The cookies that we use at SeatZac allow our website to work and help us to understand what information and advertising is most useful to visitors. Please take a moment to familiarise yourself with our cookie practices and let us know if you have any questions by sending us an email or submitting a request through the “Contact” button on our websites. We have tried to keep this Cookie Policiy as simple as possible, but if you’re not familiar with terms, such as cookies, IP addresses, and browsers, then read about these key terms first.

<b>What Does Cookie Mean?</b><br />
Cookies, pixel tags and similar technologies (collectively ‘cookies’) are files containing small amounts of information which are downloaded to any internet enabled device – such as your computer, smartphone or tablet – when you visit a website. Cookies are then sent back to the originating website on each subsequent visit, or to another website that recognises that cookie. Cookies do lots of different and useful jobs, such as remembering your preferences, generally improving your online experience, and helping us to offer you the best product and services. There are many types of cookies. They all work in the same way, but have minor differences. For a detailed list of cookies used on our websites, please refer to the below relevant section.

<b>Who Is Collecting It?</b><br />
Any personal data provided to or collected by SeatZac via cookies and other tracking technologies is controlled by SeatZac, the data controller.

This Cookie Policiy applies to any websites, apps, branded pages on third-party platforms (such as Facebook or YouTube), and applications accessed or used through such websites or third-party platforms (hereinafter, “our websites”) which are operated by or on behalf of SeatZac. References to “SeatZac” in this Policiy include any SeatZac companies, subsidiaries, affiliates, joint ventures and franchises that you are interacting with or have a business relationship with.

By using our websites, you are consenting to our use of cookies in accordance with this Cookie Policiy and our Privacy Policiy. If you do not agree to our use of cookies in this way, you should set your browser settings accordingly, disable the cookies that we use or not use our websites at all. If you disable the cookies we use, this may impact your user experience while on the websites.

The section below summarises the different types of cookies we use on our websites, together with their respective purpose and provides you with the ability to manage these cookies.SeatZac will only collect, use or disclose your personal data where it is fair and lawful to do so. For a more detailed understanding of how we use personal data collected by SeatZac cookies, please refer to our Privacy policy.

<b>What Purpose Do We Use Cookies For?</b><br />
We use cookies to make SeatZac websites easier to use, to deliver a personalised experience on our websites, and to better tailor our products, services and websites to your interests and needs. Cookies are used to help speed up your future activities and your experience on SeatZac websites.
The cookies you consent to, are also used to collect your personal data which we then profile into audiences so that we can deliver targeted advertising tailored to your interests and limit the number of times you see an advertisement. For more detailed information about the profiling activities SeatZac undertakes with your personal data for advertising, please see our Privacy Policiy.

We also insert cookies in emails and newsletters to improve our content and advertising.
Lastly, we use cookies to compile anonymous, aggregated statistics that allow us to understand how people use our websites and to help us improve their structure and content and also help us measure the effectiveness of advertising campaigns on SeatZac and non-SeatZac websites.

More detailed information about the types of cookies we use and for what purposes, can be found in the below relevant section.

<b>How Can I Control Or Delete Cookies?</b>
<br /><br />
<b>Here are many ways to manage your cookies:</b><br />

<ul>
<li><p>You can refuse your consent</p></li>
<li><p>You can disable SeatZac or third-party cookies by use of your browser settings</p></li>
<li><p>ou can use a cookie management tool to disable SeatZac or third-party cookies</p></li>
</ul>
