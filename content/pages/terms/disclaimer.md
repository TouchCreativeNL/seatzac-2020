---
title: Disclaimer
---

The private company Zigzac BV offers the utmost care for the safety and reliability of its products. However, a manufacturing error can always occur.

Zigzac BV, the private company, its employees, representatives, licensees, trade partners and the author of this disclaimer are not liable for any damage which occurs or may occur as a result of (a) production error(s) or use of the SeatZac that conflicts with the user instructions below.

Zigzac BV is also not liable for any indirect damage suffered by the user of the SeatZac or third parties, including consequential damages, loss of sales and profits and immaterial damage. Zigzac BV is not liable for any damage suffered as a result of the use of information, advice or ideas provided by or on behalf of Zigzac BV in brochures, advertising leaflets, or on their websites or the websites of representatives, licensees and trade partners.

Improper use of the SeatZac may cause damage or injury to others and may constitute a violation of intellectual property rights, regulations regarding privacy, publication, and/or communication in the broadest sense of the word. You are solely responsible for the use of the SeatZac.
<br /><br />

<b>For the safe use of the SeatZac, you are advised to:</b>

<ul>
<li><p>Not to jump on the SeatZac</p></li>
<li><p>Not expose the SeatZac or place it too close to heat sources such as fireplaces or (open) flames</p></li>
<li><p>Be careful with sharp objects and sharp clothing items.</p></li>
<li><p>Disallow children under 6 years old to use the SeatZac.</p></li>
<li><p>Disallow jumping off of objects or into objects with the SeatZac, such as slides or swimming pools.</p></li>
<li><p>Not exceed a load of 120 kg on the SeatZac.</p></li>
<li><p>Only sit on the seating surface of the SeatZac.</p></li>
</ul>
<br /><br />

You will protect and indemnify Zigzac BV and its employees, representatives, licensees, trade partners and the author of this disclaimer against judicial and extrajudicial measures, rulings, etc., including the costs for legal assistance, accountants, etc. as set by third parties as a result of or related to your use of the ZigZac or your violation of any legal regulation whatsoever or the (intellectual property) rights of third parties.
<br /><br />

<b>Zigzac BV always reserves the right to:</b>

<ul>
<li><p>Make changes to its products, including their functionality and design, at its sole discretion without prior notice</p></li>
<li><p>Make changes to this disclaimer at its sole discretion without prior notice.</p></li>
<li><p>This Disclaimer is governed by Dutch law, and insofar as national or international legal regulations do not prescribe otherwise, all disputes will be submitted to the competent court in Groningen.</p></li>
</ul>
