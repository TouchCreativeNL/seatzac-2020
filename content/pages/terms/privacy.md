---
title: Privacy
---

Our store respects your privacy. The Privacy Policy provides concisely the manner in which your data is collected and used by Seatzac. You are advised to please read the Privacy Policy carefully. By accessing the services provided by Seatzac on our website www.seatzac.com you agree to the collection and use of your data by Seatzac in the manner provided in this Privacy Policy. This privacy policy applies to the website and all products and services offered by Seatzac.

<b>We want you to:</b>

<ul>
<li><p>Feel comfortable using our website</p></li>
<li><p>Feel secure submitting information to us</p></li>
<li><p>Contact us with your questions or concerns about privacy on this site</p></li>
<li><p>Know that by using our sites you are consenting to the collection of certain data</p></li>
</ul>
<br /><br />

<b>What information is, or may be, collected from you?</b><br />
We will automatically receive and collect certain anonymous information in standard usage logs through our Web server, including computer-identification information obtained from “cookies,” sent to your browser from:

<ul>
<li><p>Web server cookie stored on your hard drive</p></li>
<li><p>An IP address, assigned to the computer which you use</p></li>
<li><p>The domain server through which you access our service</p></li>
<li><p>The type of computer you’re using</p></li>
<li><p>The type of web browser you’re using</p></li>
</ul>
<br /><br />

A cookie is a small text file that is stored on a user’s computer for record-keeping purposes. We use cookies on this site. We do not link the information we store in cookies to any personally identifiable information you submit while on our site. The use of cookies by third parties is not covered by our privacy policy. We do not have access or control over these cookies.
<br /><br />

<b>We may collect the following personally identifiable information about you for orders:</b>

<ul>
<li><p>Name including first and last name</p></li>
<li><p>Email addresses</p></li>
<li><p>Phone numbers & contact details</p></li>
<li><p>Other information as per our registration process</p></li>
<li><p>Opinions of features on our websites</p></li>
</ul>
<br /><br />

<b>We may also collect the following information:</b>

<ul>
<li><p>About the pages you visit/access</p></li>
<li><p>The links you click on our site</p></li>
<li><p>The number of times you access the page</p></li>
<li><p>The number of times you have shopped on our web site</p></li>
</ul>
<br /><br />

<b>Access to Personally Identifiable Information:</b><br />
If your personally identifiable information changes, or if you no longer desire our service, you may correct, update, amend or request deletion by making the change on our account information page or by emailing customer support at info@seatzac.com. We will retain your information as needed to provide you services. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.

<b>Who collects the information?</b><br />
We will collect anonymous traffic information from you when you visit our site. We will collect personally identifiable information about you only as part of a voluntary registration process, on-line survey, or contest or any combination thereof. This privacy policy applies only to information collected by this Website.
<br /><br />

<b>How is the information used?</b>

<b>We use your personal information to:</b>

<ul>
<li><p>Help us provide personalized features</p></li>
<li><p>Tailor our sites to your interest</p></li>
<li><p>To get in touch with you when necessary or respond to your inquiry</p></li>
<li><p>To provide the services requested by you</p></li>
<li><p>To preserve social history as governed by existing law or policy</p></li>
<li><p>To conduct surveys</p></li>
</ul>
<br /><br />

<b>We use contact information internally to:</b>

<ul>
<li><p>Direct our efforts for product improvement</p></li>
<li><p>Contact you as a survey respondent</p></li>
<li><p>Notify you if you win any contest</p></li>
<li><p>Send you promotional offer information</p></li>
</ul>
<br /><br />

<b>Generally, we use anonymous traffic information to:</b>

<ul>
<li><p>Recognize your access privileges to our Websites</p></li>
<li><p>Help diagnose problems with our server</p></li>
<li><p>Administer our websites, track your session so that we can understand better how people use our sites</p></li>
</ul>
<br /><br />

<b>With whom will your information be shared?</b><br />
We will not use or store your financial information for any purpose other than to complete a transaction with you. We do not rent, sell or share your personal information and we will not disclose any of your personally identifiable information to third parties unless:

<ul>
<li><p>To provide products or services you’ve requested</p></li>
<li><p>To help investigate, prevent or take action regarding unlawful and illegal activities, suspected fraud, potential threat to the safety or security of any person, violations of Coffee Day’s terms of use or to defend against legal claims</p></li>
<li><p>To bill you for goods and services</p></li>
<li><p>As required by law or Government/Statutory Authority or to comply with summons / subpoena or any legal process</p></li>
<li><p>When we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request</p></li>
</ul>
<br /><br />

What choices are available to you regarding collection, use and distribution of your information?<br />
You may change your interests at any time and may opt-in or opt-out of any marketing / promotional / newsletters mailings. Seatzac reserves the right to send you certain service related communication, considered to be a part of your Seatzac account without offering you the facility to opt-out. You may update your information and change your account settings at any time.

What security procedures are in place to protect information from loss, misuse or alteration?<br />
To protect against the loss, misuse and alteration of the information under our control, we have in place appropriate physical, electronic and managerial procedures. For example, our servers are accessible only to authorized personnel and that your information is shared with respective personnel on need to know basis to complete the transaction and to provide the services requested by you. Although we will endeavor to safeguard the confidentiality of your personally identifiable information, transmissions made by means of the Internet cannot be made absolutely secure. By using seatzac.com you agree that we will have no liability for disclosure of your information due to errors in transmission or unauthorized acts of third parties.

<b>Policy updates</b><br />
We may update this privacy policy to reflect changes to our information practices. We reserve the right to change or update this policy at any time at our discretion and by placing a prominent notice on our website. Such changes shall be effective immediately upon posting to this site.

<b>Governing Law</b><br />
Seatzac.com will not accept any form of liability due the use of our website or products. These Online Shopping Terms and Conditions, the use of our website and any order or purchase made through the facilities of our website shall be governed by the laws of The Netherlands.
