---
__id: 1
theme: blue
header: header.jpg
video: e-zy-video
category: e-zy
product: Seatzac E-ZY&trade;
title: Seatzac E-ZY&trade;
price: 69.99
description: The super compact and comfortable chill bag. From now, you can fill the 400 litres of air within a minute, with one simple touch of a button. You can chill wherever you want!
title_first: Chill in a minute
title_second:
image1: ezy_1.jpg
image2: ezy_2.jpg
image_all: ezy-all.jpg
text_1: SeatZac is the leading brand in Europe when it comes down to air loungers and developed the first SELF inflatable Chill Chair in the world! A very comfortable chair with a patented and integrated air pump (MR E-ZY) that can help you quickly inflate our SeatZac products within 60 seconds. From now you don’t need a lot of space and wind anymore.
text_2: Once your SeatZac is fully inflated the MR E-ZY is even capable to charge your mobile phone and other devices because of the internal power bank.
text_3: You can chill wherever you want… your living room, the balcony or the park. Click and chill, because it’s internal battery takes care of the rest. SeatZac is made of strong polyester (known as parachute silk) weighing super light. Easy to carry in the included carrier bag and available in 4 different colors.
text_4: SeatZac E-ZY, That’s chill and shine at the same time!
productinformation:
  [
    {
      _id: 1,
      title: Properties,
      text: SeatZac is made of durable and tear-resistant 210T light-weight polyester and is easy to clean with just water and soap.,
    },
    { _id: 2, title: Dimensions, text: 110 x 75 x 58 cm },
  ]
---
