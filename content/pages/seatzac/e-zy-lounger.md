---
__id: 1
theme: green
header: header.jpg
video: lounger-video
category: e-zy-lounger
product: Seatzac E-ZY Lounger&trade;
title: Seatzac E-ZY Lounger&trade;
price: 69.99
description: The most comfortable Self Inflatable chill bag in the world! Just one simple touch of a button and you can chill, relax and lay back wherever you want!
title_first: Premium relaxing
title_second:
image1: ezy_lounger_1.jpg
image2: ezy_lounger_2.jpg
image_all: lounger-all.jpg
text_1: SeatZac is the leading brand in Europe when it comes down to air loungers and developed the first SELF inflatable Relaxing Chair in the world! A very comfortable chair with a patented and integrated air pump (MR E-ZY) that can help you quickly inflate our SeatZac products within 60 seconds. From now you don’t need a lot of space and wind anymore.
text_2: Once your SeatZac is fully inflated the MR E-ZY is even capable to charge your mobile phone and other devices because of the internal power bank.
text_3: With our ergonomic curve design, the SeatZac is giving you great support to your back and neck when you sit down. It is very stable and not easy to roll over.
text_4: This is what you call ‘Premium Relaxing within a Minute!’ SeatZac is made of strong polyester (known as parachute silk) weighing super light. Easy to carry in the included carrier bag and available in 4 different colors.
productinformation:
  [
    {
      _id: 1,
      title: Properties,
      text: SeatZac is made of durable and tear-resistant 210T light-weight polyester and is easy to clean with just water and soap.,
    },
    { _id: 2, title: Dimensions, text: 110 x 75 x 58 cm },
  ]
---
