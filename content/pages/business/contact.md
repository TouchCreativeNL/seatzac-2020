---
title: Contact
---

SeatZac Global<br />
Mail: info@seatzac.com<br />
Tel: <a href="callto:+31 652 014 707">+31 652 014 707</a><br />
Facebook: www.facebook.com/seatzac<br />
Instagram: www.instagram.com/seatzac<br />

SeatZac Nederland<br />
Zigzac BV<br />
Kamer van Koophandel 66465818<br />
Brantjesstraat 72<br />
1447 PE Purmerend<br />
Mail: info@seatzac.com<br />

Distributor Retail Europe<br />
Bolan Home Fashion GmbH<br />
Prozessionsweg 26<br />
48493 Wettringen<br />
GERMANY<br />
Mail: sales@bolan-home-fashion.de<br />

Retail en webshop<br />
Mail: reind@seatzac.com<br />
Tel: <a href="callto:+31 652 014 707">+31 652 014 707</a><br />

SeatZac as a promotional item<br />
Brandhouse
<br />
www.brandhouse.nl
<br />
Tel: <a href="callto:+31 76 - 205 93 63">+31 76 - 205 93 63</a>
<br />
Website: www.brandhouse.nl
<br />
Mail: sales@brandhouse.nl
