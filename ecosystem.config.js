module.exports = {
  apps: [
    {
      name: 'Seatzac',
      script: 'npm',
      args: 'start',
      autorestart: true,
      watch: ['server', 'client'],
      watch_delay: 5000,
      ignore_watch: ['node_modules'],
      watch_options: {
        followSymlinks: false,
      },
      max_memory_restart: '200M',
    },
  ],
}
